#include <iostream>
#include <fstream>
#include <unistd.h>
#include <string.h>
#include <vector>
#include <algorithm>

int filesize(const char* filename)
{
    std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    return in.tellg(); 
}

class File {

public:
    int size;
    int offset;
    std::string name;

    File(std::string name, int size, int offset) : name(name), size(size), offset(offset){}

    File(std::string info){
        size = atoi(info.substr(info.find(",") + 1, info.find_last_of(",")).c_str());
        offset = atoi(info.substr(info.find_last_of(",") + 1, info.length() - 1).c_str());
        name = info.substr(0, info.find(","));
    }

    bool operator<(const File& r)
    {
        return offset < r.offset;
    }
};

class FS {
public:
    std::fstream file;
    std::vector<File> files;
    int firstFileOffset;
    int filesLimit;
    int size;

    FS(std::string path){
        file.open(path, std::fstream::in | std::fstream::out);
        readInfo();
    }

    FS(const FS &fs){}

    bool addFile(std::string path){
        if(files.size() == filesLimit){
            return false;
        }

        for(auto file : files){
            if(file.name == path) return false;
        }

        std::fstream fileToAdd;
        fileToAdd.open(path, std::fstream::in);

        int fileSize = filesize(path.c_str());
        int fileOffset = 0;
        int a = files.size() - 1;

        for(int i=0; i < a; i++){
            if(files[i].offset + files[i].size + fileSize < files[i+1].offset){
                fileOffset = files[i].offset + files[i].size;
                break;
            }
        }
        if(files.size() == 0 && fileSize <= size) {
            fileOffset = firstFileOffset;
        }
        if (!fileOffset && files.back().offset + files.back().size + fileSize < size){
            fileOffset = files.back().offset + files.back().size;
        }

        if(fileOffset){
            files.push_back(File(path, fileSize, fileOffset));
            saveFilesInfo();

            file.seekp(fileOffset);

            std::string buf;
            while(getline(fileToAdd, buf)){
                file << buf << std::endl;
            }
        }
        
        fileToAdd.close();
        return fileOffset != 0;
    }

    bool readFile(std::string source, std::string destination){
        for(auto file : files){
            if(file.name == source){
                std::fstream destinationFile;
                destinationFile.open(destination, std::fstream::out);

                char buf[file.size + 1];
                this->file.seekg(file.offset);
                this->file.read(buf, file.size);
                buf[file.size ] = 0;
                destinationFile << buf;
                destinationFile.close();
                return true;
            }
        }
        return false;
    }

    void deleteFile(std::string name){
        std::vector<File> buf;
        for (auto file : files) {
            if(file.name != name) buf.push_back(file);
        }
        files = buf;
        saveFilesInfo();
    }

    void readInfo(){
        std::string buf;
        std::getline(file, buf);
        filesLimit = atoi(buf.substr(0, buf.find(",")).c_str());
        size = atoi(buf.substr(buf.find(",") + 1, buf.find_last_of(",")).c_str());
        firstFileOffset = atoi(buf.substr(buf.find_last_of(",") + 1, buf.length() - 1).c_str());

        readFilesInfo();
    }

    void printDirectory(){
        std::cout << "name  size" << std::endl;
        for (auto file : files){
            std::cout << file.name << " " << file.size << std::endl;
        }
    }

    void printDiskUsage(){
        int usage = firstFileOffset;

        std::cout << "Type  size" << std::endl;
        std::cout << "Directory " << firstFileOffset << std::endl;


        for(int i=0; i < files.size(); i++){
            usage += files[i].size;
            if(i == 0) {
                if(files[0].offset != firstFileOffset){
                    std::cout << "Free " << files[0].offset - firstFileOffset << std::endl;
                }
            }
            std::cout << files[i].name << " " << files[i].size << std::endl;
            
            if (i == files.size() - 1) {
                if(files[i].size + files[i].offset != size){
                    std::cout << "Free " << size - files[i].offset - files[i].size << std::endl;
                }
            } else {
                if(files[i].size + files[i].offset != files[i+1].offset){
                    std::cout << "Free " << files[i+1].offset - files[i].offset - files[i].size << std::endl;
                }
            }
        }
        std::cout << "Usage: " << usage << "/" << size << std::endl;
    }

    void readFilesInfo(){
        files.clear();

        std::string buf;
        for(int i=0; i<filesLimit; i++){
            std::getline(file, buf);
            if(buf.length() == 0 || (buf[0] == ',' && buf[1] == ',')) continue;
            files.push_back(File(buf));
        }

        std::sort(files.begin(), files.end());
    }

    void saveFilesInfo(){
        std::string buf;

        file.seekg(0);
        std::getline(file, buf);
        file.seekp(file.tellg());
        for(File file : files){
            this->file << file.name << "," << file.size << "," << file.offset << std::endl;
        }

        for(int i=0; i<filesLimit - files.size();i++) this->file << std::endl;
    }

    ~FS(){
        file.close();
    }

    static void initFSFile(std::string path, int size = 1024 * 1024, int filesLimit = 100, int firstFileOffset = 20 * 1024){
        std::fstream file;
        file.open(path, std::fstream::out);

        if(truncate(path.c_str(), size)){
            std::cout << "Error." << std::endl;
        } else {
            std::cout << "Created!" << std::endl;
        }
        
        file << filesLimit << "," << size << "," << firstFileOffset << std::endl;
        for(int i=0; i<filesLimit; i++){
            file << std::endl;
        }
        
        file.close();
    }

    static bool deleteFSFile(std::string path){
        return remove(path.c_str()) == 0;
    }
};

int main(int argc, char *argv[]){
    if(argc < 2) {
        std::cout << "Arg expected" << std::endl;
        return 0;
    }

    if (strcmp(argv[1], "create") == 0) {
        std::cout << "Creating..." << std::endl;

        if(argc < 4){
            std::cout << "3 args expected" << std::endl;
            return 0;
        }

        FS::initFSFile(argv[2], atoi(argv[3]) * 1024);
    }
    else if (strcmp(argv[1], "cp") == 0) {
        std::cout << "Copying to disk..." << std::endl;

        if(argc < 4){
            std::cout << "3 args expected" << std::endl;
            return 0;
        }

        auto fs = FS(std::string(argv[2]));

        if(fs.addFile(argv[3])){
            std::cout << "Copying completed" << std::endl;
        } else {
            std::cout << "Copying failed" << std::endl;
        }

    } else if (strcmp(argv[1], "rcp") == 0) {
        std::cout << "Copying from disk..." << std::endl;

        if(argc < 5){
            std::cout << "4 args expected" << std::endl;
            return 0;
        }

        auto fs = FS(std::string(argv[2]));
        if(fs.readFile(argv[3], argv[4])){
            std::cout << "Copying completed" << std::endl;
        }   else {
            std::cout << "Copying failed" << std::endl;
        }
    } else if (strcmp(argv[1], "delete") == 0) {
        std::cout << "Deleting..." << std::endl;

        if(argc < 3){
            std::cout << "2 args expected" << std::endl;
            return 0;
        }

        if(FS::deleteFSFile(std::string(argv[2]))){
            std::cout << "Deleted" << std::endl;
        } else {
            std::cout << "Deleting failed" << std::endl;
        }
    } else if (strcmp(argv[1], "rm") == 0) {
        std::cout << "Deleting file..." << std::endl;

        if(argc < 4){
            std::cout << "3 args expected" << std::endl;
            return 0;
        }

        auto fs = FS(std::string(argv[2]));
        fs.deleteFile(argv[3]);
    } else if (strcmp(argv[1], "ls") == 0) {
        if(argc < 3){
            std::cout << "2 args expected" << std::endl;
            return 0;
        }

        auto fs = FS(std::string(argv[2]));
        fs.printDirectory();
    } else if (strcmp(argv[1], "du") == 0) {
        if(argc < 3){
            std::cout << "2 args expected" << std::endl;
            return 0;
        }

        auto fs = FS(std::string(argv[2]));
        fs.printDiskUsage();
    }
    return 0;
}

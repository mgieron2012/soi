#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

struct Command {
    int command_code;
    int buffer_index;
    int optional_arg;
};

union DataUnion
{
    unsigned long int i;
    double d;
};

unsigned long int fibonacci(unsigned i){
    if(i < 3) return 1;
    return fibonacci(i-1) + fibonacci(i-2);
}

struct Command *buf;
union DataUnion data[2];

sem_t buf_in_use;

void* thread_fib(void* arg)
{
    struct Command *comm;
    unsigned i;
    i = 0;

    while(1){
        sleep(1);
        i++;
        comm = malloc(sizeof(*comm));
        comm->buffer_index = 0;
        comm->command_code = 0;
        comm->optional_arg = i % 20 + 1;

        sem_wait(&buf_in_use);
        data[0].i = 0;
        buf = comm;

        while (data[0].i == 0);
        sem_post(&buf_in_use);
        printf("%lu \n", data[0].i);
    }
}

void* thread_rand(void* arg)
{
    struct Command *comm;

    while(1){
        sleep(2);
        comm = malloc(sizeof(*comm));
        comm->buffer_index = 1;
        comm->command_code = 1;

        sem_wait(&buf_in_use);
        data[1].d = -1;
        buf = comm;

        while (data[1].d == -1);        
        sem_post(&buf_in_use);
        printf("%f \n", data[1].d);
    }
}

int main()
{
    pthread_t p_fib, p_rand;
    srand(time(NULL));
    sem_init(&buf_in_use, 0, 1);

    pthread_create(&p_fib, NULL, thread_fib, NULL);
    pthread_create(&p_rand, NULL, thread_rand, NULL);

    while(1){
        while (buf == NULL);

        if (buf->command_code == 0) data[buf->buffer_index].i = fibonacci(buf->optional_arg);
        else data[buf->buffer_index].d = (double)rand() / 1000;

        free(buf);
        buf = NULL;
    }

    sem_destroy(&buf_in_use);
    return 0;
}
